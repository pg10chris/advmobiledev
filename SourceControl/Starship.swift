//
//  Starship.swift
//  SourceControl
//
//  Created by Christopher (Chris) Raedeke on 2017-12-06.
//  Copyright © 2017 Christopher (Chris) Raedeke. All rights reserved.
//

import Foundation

class Starship {
    
    let name: String
    let crew: Int
    
    init(name: String, crew: Int) {
        self.name = name
        self.crew = crew
    }
}
