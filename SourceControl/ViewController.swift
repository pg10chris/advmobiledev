//
//  ViewController.swift
//  SourceControl
//
//  Created by Christopher (Chris) Raedeke on 2017-12-06.
//  Copyright © 2017 Christopher (Chris) Raedeke. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    

    var starships = [Starship]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "categoryCells")
        
        getStarshipsFromAPI()
    }
    
    
    func getStarshipsFromAPI() {
        
        let myUrl = URL(string: "https://www.swapi.co/api/starships/9/")!
        let myRequest = URLRequest(url: myUrl)
        let session = URLSession.shared
        let task = session.dataTask(with: myRequest) {
            (data, response, error) -> Void in
            
            if let data = data, let json = try?
            JSONSerialization.jsonObject(with: data, options: []),
                let dict = json as? [String: Any] {
                if let resultsArray = dict["results"] as? [[String: Any]] {
                    for result in resultsArray {
                        if let name = result["name"] as? String {
                            //Send to Label in Tableview
                            print(name)
                        }
                        if let crew = result["crew"] as? Int {
                            //Send to Label in Tableview
                            print(crew)
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return starships.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCells", for: indexPath) as! TableViewCell
        let category = starships[indexPath.row]
        cell.myLabel.text = category.name
        return cell
    }
}

